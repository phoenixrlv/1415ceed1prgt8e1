package vista;

import controlador.Controlador;
import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JFrame;
import modelo.Alumno;

/**
 * Fichero: VistaTerminal.java
 *
 * @author Paco Aldarias <paco.aldarias@ceedcv.es>
 * @date 21-oct-2014
 */
public class VistaTerminal extends JFrame implements Vista {

    private Controlador controlador;
    // Gestión de la entrada por teclado
    private BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    public static boolean validarEmail(String s) {
        Pattern p = Pattern.compile("[\\w\\.]+@\\w+\\.\\w+");
        Matcher m = p.matcher(s);
        return m.matches();
    }

    public Alumno pedirAlumno() {

        Alumno a;
        String nombre = "";
        String email = "";
        String linea = "";
        String edad_;
        int edad = 0;
        boolean error = true;
        boolean esvalido = true;
        String id;

        System.out.println("ENTRADA DE DATOS");

        System.out.print("Nombre: ");
        try {
            nombre = leerLinea();
        } catch (IOException ex) {
            Logger.getLogger(VistaTerminal.class.getName()).log(Level.SEVERE, null, ex);
        }

        while (error == true) {

            System.out.print("Edad: ");
            try {
                edad_ = leerLinea();
                edad = Integer.parseInt(edad_);
                error = false;
            } catch (IOException ex) {
                Logger.getLogger(VistaTerminal.class.getName()).log(Level.SEVERE, null, ex);

            } catch (NumberFormatException nfe) {
                System.out.println("Error: Se debe introducir un número");
                error = true;
            }

        } // while

        error = true;
        while (error == true) {

            System.out.print("Email: ");
            try {
                email = leerLinea();
                esvalido = validarEmail(email);
                if (esvalido) {
                    error = false;
                } else {
                    System.out.println("Error: Email no valido");
                    error = true;
                }

            } catch (IOException ex) {
                Logger.getLogger(VistaTerminal.class.getName()).log(Level.SEVERE, null, ex);

            }

        } // while

        a = new Alumno("", nombre, edad, email);

        return a;
    }

    public void mostrarAlumno(Alumno alumno) {

        System.out.println("SALIDA DE DATOS");
        System.out.println("Id: " + alumno.getId());
        System.out.println("Nombre: " + alumno.getNombre());
        System.out.println("Edad: " + alumno.getEdad());
        System.out.println("Email: " + alumno.getEmail());

    }

    private String leerLinea() throws IOException {

        InputStreamReader isr = new InputStreamReader(System.in);
        BufferedReader br = new BufferedReader(isr);
        String linea;
        linea = br.readLine();
        return linea;
    }

    public void error() {
        System.out.println("Error: Opcion Incorrecta ");
    }

    @Override
    public void setControlador(Controlador c) {
        controlador = c;
    }

    @Override
    public void arranca() {
        try {
            procesaNuevaOperacion();
        } catch (IOException ex) {
            Logger.getLogger(VistaTerminal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private void procesaNuevaOperacion() throws IOException {

        String operacion = null;

        solicitaOperación();
        operacion = leerLinea();
        operacion = operacion.toLowerCase();

        while (!operacion.equals("e")) {

            switch (operacion) {

                case "e": // Exit
                    controlador.actionPerformed(new ActionEvent(this, 1, EXIT));
                    break;

                case "c": // Crear/AñadirAñadir
                    controlador.actionPerformed(new ActionEvent(this, 2, CREATE));
                    break;

                case "r": // Read/Leer
                    controlador.actionPerformed(new ActionEvent(this, 3, READ));
                    break;

                case "u": // Update/Actualizar
                    controlador.actionPerformed(new ActionEvent(this, 4, UPDATE));
                    break;
                case "D": // Delete / Borrar
                    controlador.actionPerformed(new ActionEvent(this, 5, DELETE));
                    break;
            }

            solicitaOperación();
            operacion = leerLinea();
            operacion = operacion.toLowerCase();
        }

    }

    private void solicitaOperación() {
        System.out.println("MENU CRUD ");
        System.out.println("e. exit ");
        System.out.println("c. create ");
        System.out.println("r. read ");
        System.out.println("u. update ");
        System.out.println("d. delete ");
        System.out.print("Opción:  ");
    }

    @Override
    public void error(String error) {
        System.out.println("Error: " + error);
    }

    @Override
    public void exit() {
        System.out.println("Fin");
    }

    @Override
    public String pedirId() {
        String id = null;
        try {
            System.out.print("Id: ");
            id = leerLinea();
            return id;
        } catch (IOException ex) {
            Logger.getLogger(VistaTerminal.class.getName()).log(Level.SEVERE, null, ex);
        }
        return id;
    }

}
